'use strict';

const readline = require('readline');

const csv = require('csvtojson'); // csv parser

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});
const csvFilePath = 'english.csv';

//***********************************Instructions********************************
/*
 * Implement a Crossword Solver
 * 1. User needs to be able to input the word length, and a combination of known letters and wildcards. 
 *  The input format should use an asterisk: '*' (U+002A) as a wildcard.
 *  Make this simple!
 *  
 * 2. The program should return all words in the dictionary that match the input pattern. 
 *      This means you won't be solving a whole crossword - just providing options for what would 
 *      fit in a crossword row or column.
 *      
 *   Example Input: bee*
 *   Output: Found 5 words in 1000 ms: Beef, Been, Beep, Beer, Bees, Beet
 *   
 *   Example Input: *eel
 *   Output: Found 8 words in 1000 ms: Feel, Heel, Keel, Peel, Reel, Seel, Teel, Weel
 *   
 * 3. An example dictionary .csv is included for your convenience. Load the words to an appropriate data structure as needed.
 * 4. Using the console.time() and console.timeEnd(), be sure to indicate how long your solution takes to load its structures, and produce a result.
 *      Hint: display the time to process a result AFTER printing them out, otherwise you won't be able to see it in large result sets.
 * 
 * A few final points:
 * 1. The order of priorities for the solution should be: Correctness, Performance, elegance, and usability.
 * 2. Startup time and memory usage are much less important than the time taken to solve the crossword pattern (which is critical).
 * 3. Error handling and input validation are "nice to haves", as long as your control schemes and instructions are clear.
 * 4. Do not change the method signature or parameters of GetPossibleWords. In other words your solution should be built around this method.
 * 
 */

/**
 * Returns a list of possible words based on the template.
 * @param {String} template The template
 * @param {Object} lookup The lookup
 * @returns {Array<String>} List of possible words
 */
function getPossibleWords(template, lookup) {
  template = "^" + template + "$"; // add anchors for start and end of string
  const regEx = new RegExp(template.replace(/\*/gm, '[a-zA-Z]{1}'),'i'); // build regexp, replacing * with regexp token and quantifier
  let matchedWords = [];
  let possWords = lookup.map(function(word){
    regEx.exec(word) !== null && matchedWords.push(word) // push matching word
  })
  return matchedWords;
}

/**
 * Creates a word lookup
 * @param {String} filename The filename
 * @returns {Object} The lookup
 */
function loadWords(filename) {
  let words = [];
  csv({
    output: 'line', // convert csv to csv line string
    ignoreEmpty: true, // ignore empty values in csv columns
    noheader: true, // no header row in csv file; first row is data
  })
  .fromFile(filename)
  .on('data',(data)=>{ // data event is emitted for each parsed line
    words.push(data.toString()); // push each words as a string instead of an array of letters
  })
  .on('error',(err)=>{
    console.log(err); // log errors to STDOUT
  })
  return words;
}

// PROOF/TEST:
console.time('loadWords');
const lookup = loadWords(csvFilePath);
console.timeEnd('loadWords');

rl.question('Enter Template: ', (template) => {
  
  console.time(`getPossibleWords`);
  const possibleWords = getPossibleWords(template, lookup);
  
  console.log('Results: ', possibleWords);

  console.log('Found ', possibleWords.length, ' words in ');
  console.timeEnd(`getPossibleWords`);

  rl.close();
});